#!/usr/bin/env python3

"""
This script prints some basic recipe information useful for debugging the
database.
"""

import satisfactory

import os


if __name__ == '__main__':
    def main():
        root = os.path.dirname(__file__)
        recipe_root = os.path.join(root, 'data', 'recipes')
        cookbook = satisfactory.Cookbook(recipe_root)

        print("Resources:")
        for i in cookbook.resources():
            print(i)

    main()

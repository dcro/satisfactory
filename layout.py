#!/usr/bin/env python3

"""
This script takes a list of target components and tries to visualise a
(hopefully efficient) factory layout to produce the components.
"""

import satisfactory

import os

from dataclasses import dataclass
from typing import Iterable, Dict


@dataclass
class entry:
    rank: int = -1

    def fold(self, rhs):
        self.rank = max(self.rank, rhs.rank)

    def advance(self):
        self.rank += 1


class layout(object):
    cache: Dict[str, entry] = dict()
    cookbook: satisfactory.Cookbook

    def __init__(self, cookbook):
        self.cookbook = cookbook

    def score(self, target: str):
        if target in self.cache:
            return self.cache[target]

        res = entry()
        for required in self.cookbook.recipes(target)[0].input:
            res.fold(self.score(required))

        res.advance()
        self.cache[target] = res
        return res


if __name__ == '__main__':
    def main():
        import argparse

        root = os.path.dirname(__file__)
        recipe_root = os.path.join(root, 'data', 'recipes')

        parser = argparse.ArgumentParser()
        parser.add_argument('--data', type=str, default=recipe_root)
        parser.add_argument('--target', type=str, nargs="*")
        parser.add_argument('--prefer', type=str, nargs="*")

        args = parser.parse_args()

        recipes = satisfactory.Cookbook(args.data)

        if args.prefer:
            for alternative in args.prefer:
                recipes.prefer(alternative)

        l = layout(recipes)
        targets = args.target if args.target else recipes.components()
        for t in targets:
            l.score(t)

        for name, data in l.cache.items():
            print(name, data)

    main()

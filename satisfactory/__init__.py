import os
import json
import logging

from typing import Dict, Generator, Iterable, NamedTuple, Set, List, Optional

from collections import defaultdict


class Recipe(NamedTuple):
    input: Dict[str, int]
    output: Dict[str, int]

    crafting_time: Optional[int] = None
    clicks: Optional[int] = None
    stage: Optional[str] = None
    requires: Optional[str] = None


class Item(NamedTuple):
    type: Set[str]
    machine: str
    recipes: List[Recipe]

    alias: Optional[str] = None

    stack_size: Optional[int] = None

    power_usage: Optional[int] = None
    size: Optional[List[int]] = None

    requires: Optional[str] = None


class Cookbook(object):
    __recipes: Dict[str, Item]

    # Maps the name of an alternative recipe to the name of the component it
    # applies to.
    __alternatives: Dict[str, str] = defaultdict(lambda: [])

    def __init__(self, root: str):
        self.__recipes = dict()

        for dirname, dirs, files in os.walk(root):
            for f in files:
                path = os.path.join(dirname, f)
                name, _ = os.path.splitext(f)

                with open(path, 'r') as src:
                    j = json.load(src)

                    try:
                        recipes = list(Recipe(**r) for r in j['recipes'])
                        j['recipes'] = recipes
                        self.__recipes[name] = Item(**j)
                    except Exception as ex:
                        logging.error(f"Could not load {name}: {ex}")

                    for recipe in self.__recipes[name].recipes:
                        if recipe.requires:
                            self.__alternatives[recipe.requires].append(name)


    def __getitem__(self, item: str) -> Dict[str, Dict]:
        return self.__recipes[item]

    def all(self) -> Iterable[str]:
        """
        :return: An iterable of all available recipe names
        """
        return self.__recipes.keys()

    def recipes(self, name: str) -> Iterable[Dict]:
        """
        :param name: The target item name
        :return: A sequence of possible recipes for the target item
        """
        return self.__recipes[name].recipes

    def is_component(self, name: str) -> bool:
        """
        :param name: The name of an item
        :return: Whether the item is a component; ie, craftable.
        """
        return 'component' in self.__recipes[name].type

    def components(self) -> Generator[str, None, None]:
        """
        :return: The names of all items that are resources.
        """
        return (i for i in self.all() if self.is_component(i))

    def is_resource(self, name: str) -> bool:
        """
        :param name: The name of an item
        :return: Whether the item must have harvested (rather than crafted)
        """
        return 'resource' in self.__recipes[name].type

    def resources(self) -> Generator[str, None, None]:
        """
        :return: The names of all items that are resources.
        """
        return (i for i in self.all() if self.is_resource(i))

    def prefer(self, alternative: str):
        def compare(x):
            if not x.requires:
                return 1
            if not x.requires == alternative:
                return 1
            return 0

        for recipe_name in self.__alternatives[alternative]:
            self.__recipes[recipe_name].recipes.sort(key=compare)
